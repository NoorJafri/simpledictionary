#Pickle is on HardDrive Python's Storing Mechanism. We have loaded a small dictionary inside it.
import pickle
# dictionary = {
#     'Tomato': 'A fruit'
# }



def main_loop():
    ask_user()
    #dump_into_dictionary_pickle(dictionary)




# This function is called to dump our results into the dictionary
def dump_into_dictionary_pickle(dump_pickle):
    with open('dictionary.pickle', 'wb') as handle:
        pickle.dump(dump_pickle, handle, protocol=pickle.HIGHEST_PROTOCOL)

# This function is called to load our dictionary from the pickle
def load_from_dictionary_pickle():
    with open('dictionary.pickle', 'r') as handle:
        dictionary = pickle.load(handle)
        return dictionary

# This display all words
def display_all_words():
    words = load_from_dictionary_pickle()
    print "======================"
    print "Words \t\t Defination"
    print "======================\n"
    for key, value in words.iteritems():
        print "{} \t\t {}".format(key,value)

# this adds new word
def add_a_word():
    print "You are about to Enter a New Word"
    word = raw_input("Please Enter a New word = ")
    defination = raw_input("Please Enter a defination=")
    if word and defination:
        words = load_from_dictionary_pickle()
        words[word] = defination
        dump_into_dictionary_pickle(words)
        print "new word added"
    else:
        print "word or defination not properly"

#t this updates a word
def update_a_word():
    print "\nYou are about to Update a Word\n"
    word = raw_input("Please Enter a Updated word = ")
    if word:
        defination = raw_input("Please Enter a defination=")
        if defination:
            words = load_from_dictionary_pickle()
            words[word] = defination
            dump_into_dictionary_pickle(words)
            print "Word udpated"
    else:
        print "word doesn't exist"


# this delete a word
def delete_a_word():
    print "You are about to Delete a Word"
    word = raw_input("Please Enter a Updated word = ")
    if word:
        try:
            words = load_from_dictionary_pickle()
            words.pop(word, None)
            dump_into_dictionary_pickle(words)
            print "Word Successfully Deleted"
        except:
            print "word doesnt exist"
    else:
        print "word doesn't exist"


# this displat words details
def display_word_details():
    print "You are about to see details of the word"
    word = raw_input("Please Enter a Updated word = ")
    if word:
        words = load_from_dictionary_pickle()
        print "======================"
        print "Words \t\t Defination"
        print "======================\n"
        for key, value in words.iteritems():
            if key == word:
                print "{} \t\t {}".format(key,value)
    else:
        print "word doesn't exist"

# This function contro's user choices
def ask_user():
    while True:
        options = {
            1: add_a_word,
            2: update_a_word,
            3: delete_a_word,
            4: display_word_details,
            5: display_all_words,
            6: exit
        }
        user_input = input("Building Dictionary . . . \n \
        Dictioanry Built. . . \n \
        Please Select One of this Topic \n \
        1. Add a new word. \n \
        2. Update an existing word \n \
        3. Delete an existing word \n \
        4. Display a word's defination\n \
        5. Display all words \n \
        6. Exit\n \
        >>")

        options[user_input]()
        


main_loop()